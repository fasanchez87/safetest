package com.fabio.safe.model

import com.google.gson.annotations.SerializedName

data class QueryFlickr(@SerializedName("photos") val photos: Photos)