package com.fabio.safe.model

import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("id") val id: String,
    @SerializedName("owner") val owner: String?,
    @SerializedName("secret") val secret: String,
    @SerializedName("server") val server: String,
    @SerializedName("farm") val farm: String,
    @SerializedName("title") val title: String?,
    @SerializedName("ispublic") val ispublic: String,
    @SerializedName("isfriend") val isfriend: String,
    @SerializedName("isfamily") val isfamily: String)
