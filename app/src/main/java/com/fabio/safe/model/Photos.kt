package com.fabio.safe.model

import com.google.gson.annotations.SerializedName

data class Photos( @SerializedName("page") val page: String = "",
                   @SerializedName("pages") val pages: String = "",
                   @SerializedName("perpage") val perpage: String = "",
                   @SerializedName("total") val total: String = "",
                   @SerializedName("photo") val photo: List<Photo>)