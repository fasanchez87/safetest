package com.fabio.safe.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fabio.safe.app.Injection
import com.fabio.safe.app.toast
import com.fabio.safe.model.QueryFlickr
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

object RemoteRepository: Repository {

    private val api = Injection.provideVestraApi()
    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.IO

    private val scope = CoroutineScope(coroutineContext)


    override fun getPhotos(): LiveData<QueryFlickr> {
        val liveData = MutableLiveData<QueryFlickr>()
        scope.launch{
            api.getPhotos().enqueue(object : Callback<QueryFlickr> {
                override fun onResponse(call: Call<QueryFlickr>?, response: Response<QueryFlickr>?) {
                    if (response != null && response.isSuccessful) {
                        liveData.value = response.body()
                    } else {
                        toast("Error Response!")
                    }
                }

                override fun onFailure(call: Call<QueryFlickr>, t: Throwable) {
                    // liveData.value = Result.error(ApiError.REPOS, null)
                    toast("Error!")
                }
            })
        }
        return liveData
    }

    override fun cancelAllRequests() = coroutineContext.cancel()
}