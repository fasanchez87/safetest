package com.fabio.safe.repository

import androidx.lifecycle.LiveData
import com.fabio.safe.model.QueryFlickr

interface Repository {
    fun getPhotos(): LiveData<QueryFlickr>
    fun cancelAllRequests()
}