package com.fabio.safe.repository

import com.fabio.safe.app.API_KEY
import com.fabio.safe.app.FORMAT
import com.fabio.safe.app.JSONCALLBACK
import com.fabio.safe.app.METHOD
import com.fabio.safe.model.QueryFlickr
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SafeDXCApi {
    @GET("/services/rest/")
    fun getPhotos(@Query("method") method: String = METHOD,
                  @Query("nojsoncallback") nojsoncallback: String = JSONCALLBACK,
                  @Query("format") format: String = FORMAT,
                  @Query("api_key") api_key: String = API_KEY
                  ): Call<QueryFlickr>
}
