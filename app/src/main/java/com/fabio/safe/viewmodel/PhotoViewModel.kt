package com.fabio.safe.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fabio.safe.model.QueryFlickr
import com.fabio.safe.repository.RemoteRepository
import com.fabio.safe.repository.Repository

class PhotoViewModel(private val repository: Repository = RemoteRepository): ViewModel(){

    fun getPhotos(): LiveData<QueryFlickr> {
        return repository.getPhotos()
    }

    fun cancelRequestEventos(){
        repository.cancelAllRequests()
    }
}