package com.fabio.safe.app

import android.app.Application
import android.content.Context

class SafeDXCApp: Application() {

    companion object {
        private lateinit var instance: SafeDXCApp
        fun getAppContext(): Context = instance.applicationContext

        fun validateText(value: String?) =
            if (value.isNullOrEmpty()) "Not value." else value
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

}