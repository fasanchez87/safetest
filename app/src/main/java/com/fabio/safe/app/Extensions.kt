package com.fabio.safe.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.bumptech.glide.Glide

fun toast(message: String?) {
    Toast.makeText(SafeDXCApp.getAppContext(), message, Toast.LENGTH_LONG).show()
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ImageView.loadImage(url: String?){
    Glide.with(this)
        .load(url)
        .into(this)
}

