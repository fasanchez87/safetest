package com.fabio.safe.app

import com.fabio.safe.BuildConfig
import com.fabio.safe.repository.SafeDXCApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Injection {


    private fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(provideOkhhtpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun provideVestraApi(): SafeDXCApi{
        return provideRetrofit().create(SafeDXCApi::class.java)
    }

    private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if(BuildConfig.DEBUG){
            HttpLoggingInterceptor.Level.BODY
        }
        else{
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    private fun provideOkhhtpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(provideLoggingInterceptor())
        return httpClient.build()
    }
}