package com.fabio.safe.ui

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fabio.safe.R
import com.fabio.safe.app.SafeDXCApp.Companion.validateText
import com.fabio.safe.app.inflate
import com.fabio.safe.app.loadImage
import com.fabio.safe.model.Photo
import com.fabio.safe.model.QueryFlickr
import kotlinx.android.synthetic.main.photo_row.view.*


class PhotoAdapter(private val photos: MutableList<Photo>, private val clickListener: (Photo) -> Unit):
    RecyclerView.Adapter<PhotoAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.photo_row))
    }

    override fun getItemCount() = photos.size

    fun updatePhotos(photos: QueryFlickr){
        this.photos.clear()
        this.photos.addAll(photos.photos.photo)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(photos[position], clickListener)
    }

    inner class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        private lateinit var photo: Photo

        fun bind(photo: Photo, clickListener: (Photo) -> Unit){
            this.photo = photo
            itemView.title.text = validateText(photo.title)
            itemView.owner.text = validateText(photo.title)
            itemView.image.loadImage("https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg")
            itemView.setOnClickListener { clickListener(photo)}
        }
    }
}