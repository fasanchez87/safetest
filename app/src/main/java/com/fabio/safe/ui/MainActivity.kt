package com.fabio.safe.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fabio.safe.R
import com.fabio.safe.app.toast
import com.fabio.safe.model.Photo
import com.fabio.safe.model.QueryFlickr
import com.fabio.safe.viewmodel.PhotoViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity


class MainActivity : AppCompatActivity() {

    private lateinit var photosViewModel: PhotoViewModel
    private val adapter = PhotoAdapter(mutableListOf()){
        navigateToDetail(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        photosViewModel = ViewModelProviders.of(this).get(PhotoViewModel::class.java)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter

        photosViewModel.getPhotos().observe(this, Observer<QueryFlickr> { photo ->
            if(photo != null){
                adapter.updatePhotos(photo)
                progressbar.visibility = View.GONE
                recycler.visibility = View.VISIBLE
            }
            else{
                toast("Error gettings events!")
            }
        })
    }

    private fun navigateToDetail(it: Photo) {
        startActivity<DetailPhoto>(DetailPhoto.ID to it.id,
                                            DetailPhoto.AUTHOR to it.owner,
                                            DetailPhoto.FARM to it.farm,
                                            DetailPhoto.SECRET to it.secret,
                                            DetailPhoto.SERVER to it.server,
                                            DetailPhoto.TITLE to it.title)
    }
}
