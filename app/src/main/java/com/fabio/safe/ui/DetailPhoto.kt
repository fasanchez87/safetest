package com.fabio.safe.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fabio.safe.R
import com.fabio.safe.app.SafeDXCApp.Companion.validateText
import com.fabio.safe.app.loadImage
import kotlinx.android.synthetic.main.activity_detail_photo.*

class DetailPhoto : AppCompatActivity() {

    companion object{
        const val ID: String = "ID"
        const val TITLE: String = "TITLE"
        const val AUTHOR: String = "AUTHOR"
        const val FARM: String = "AUTHOR"
        const val SERVER: String = "SERVER"
        const val SECRET: String = "SECRET"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photo)

        val id = intent.getStringExtra(ID)
        val title: String? = intent.getStringExtra(TITLE)
        val author: String? = intent.getStringExtra(AUTHOR)

        val farm = intent.getStringExtra(FARM)
        val server = intent.getStringExtra(SERVER)
        val secret = intent.getStringExtra(SECRET)

        supportActionBar?.title = getString(R.string.detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title_det.text = validateText(title.toString())
        owner_det.text = validateText(author.toString())
        desc.text = resources.getText(R.string.lorem)
        image_det.loadImage("https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg")
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
